#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define tam 1.0
#define dx 0.00001
#define dt 0.000001
#define T  0.01
#define kappa 0.000045

int main(int argc, char **argv) {

  double *tmp, *u, *u_prev, x, t;
  int i, n, maxloc;

  /* Calculando quantidade de pontos */
  n = tam/dx;

  /* Alocando vetores */
  u = (double *) malloc((n+1)*sizeof(double));
  u_prev = (double *) malloc((n+1)*sizeof(double));

  x = dx;
  for (i=1; i<n; i++) {
    if (x<=0.5) u_prev[i] = 200*x;
    else        u_prev[i] = 200*(1.-x);
    x += dx;
  }

  t = 0.;
#pragma omp parallel private(i)
  while (t<T) {
#pragma omp for
    for (i=1; i<n; i++) {
      u[i] = u_prev[i] + kappa*dt/(dx*dx)*(u_prev[i-1]-2*u_prev[i]+u_prev[i+1]);
    }
#pragma omp single
    {
      u[0] = u[n] = 0.; /* força condição de contorno */
      tmp = u_prev; u_prev = u; u = tmp; /* troca entre ponteiros */
      t += dt;
    }
  }

  /* Calculando o maior valor e sua localização */
  maxloc = 0;
  for (i=1; i<n+1; i++) {
    if (u[i] > u[maxloc]) maxloc = i;
  }
}
