CC=gcc
CFLAGS=-fopenmp -O3
DEPS =
OBJ = ftcs.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

ftcs: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm *.o ftcs
